import React from 'react';
import './assets/css/main.css';
import Header from './components/Header'
import Main from './components/Main'
import Player from './components/Player'

const App: React.FC = () => {
  return (
    <div className="App">
      <Header collapsed={true}/>
      <Main />
      <Player title="Tecnologería" />
    </div>
  );
}

export default App;
