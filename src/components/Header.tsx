import React from 'react';
import SearchText from './SearchText';
import {library} from '@fortawesome/fontawesome-svg-core';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import {faFilter} from '@fortawesome/free-solid-svg-icons';

library.add(faFilter);

interface IHeaderProps{
    collapsed: boolean;
}

interface IHeaderState{
    collapsed: boolean;
    searchText: string;
}

class Header extends React.Component<IHeaderProps, IHeaderState>{
    constructor(props: IHeaderProps){
        super(props);
        this.state = {collapsed:true, searchText: ""};
    }

    public onFilterClick = () => {
        this.setState({collapsed: !!!this.state.collapsed});
    }

    public onSearchTextChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({searchText: event.target.value});
    }

    public render(){
        return (
            <div className = "header">
                <p>Hola mundo: {this.state.searchText}</p>
                <a className = "btn btn-success" onClick = {this.onFilterClick}>
                    <FontAwesomeIcon icon = "filter" />
                </a>
                {!!!this.state.collapsed ?
                    <div className = "filterForm">
                        <SearchText onSearchTextChange = {this.onSearchTextChange}/>
                    </div>
                : null}
            </div>
        );
    }
}

export default Header;
